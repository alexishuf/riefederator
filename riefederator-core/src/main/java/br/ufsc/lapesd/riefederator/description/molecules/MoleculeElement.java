package br.ufsc.lapesd.riefederator.description.molecules;

import javax.annotation.Nonnull;

public interface MoleculeElement {
    @Nonnull String getName();
}
