package br.ufsc.lapesd.riefederator.model;

public class NTParseException extends Exception {
    public NTParseException(String message) {
        super(message);
    }
}
