package br.ufsc.lapesd.riefederator.webapis;

import br.ufsc.lapesd.riefederator.query.endpoint.CQEndpoint;

/**
 * A interface to mark endpoints as querying an Web API.
 *
 * @see WebAPICQEndpoint for a implementation.
 */
public interface WebApiEndpoint extends CQEndpoint {
}
